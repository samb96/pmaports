# Contributor: Thiago Foganholi <thiagaoplusplus@outlook.com>
# Maintainer: Thiago Foganholi <thiagaoplusplus@outlook.com>
# Kernel config based on: arch/arm/configs/exynos_defconfig

pkgname=linux-postmarketos-exynos4
pkgver=5.11.8
pkgrel=0
pkgdesc="Mainline kernel fork for Samsung Exynos4 devices"
arch="armv7"
_carch="arm"
_flavor="${pkgname#linux-}"
url="https://kernel.org"
license="GPL-2.0-only"
options="!strip !check !tracedeps pmb:cross-native pmb:kconfigcheck-anbox"
makedepends="bison busybox-static-$arch findutils flex gmp-dev installkernel mpc1-dev mpfr-dev openssl-dev perl xz"

# Source
_config="config-$_flavor.$arch"
case $pkgver in
	*.*.*)	_kernver=${pkgver%.0};;
	*.*)	_kernver=$pkgver;;
esac
source="
	https://cdn.kernel.org/pub/linux/kernel/v${_kernver%%.*}.x/linux-$_kernver.tar.xz
	$_config
	0001-ARM-decompressor-Flush-tlb-before-swiching-domain-0-.patch
	0005-ARM-dts-exynos-Fix-charging-regulator-voltage-and-cu.patch
	0006-ARM-dts-exynos-Add-top-off-charging-regulator-node-f.patch
	0007-extcon-max8997-Add-CHGINS-and-CHGRM-interrupt-handli.patch
	0008-power-supply-max8997_charger-Set-CHARGER-current-lim.patch
	0009-ARM-dts-exynos-Add-charger-supply-for-I9100.patch
	0010-power-supply-max8997_charger-Switch-to-new-binding.patch
	initramfs.list
	init
"
builddir="$srcdir/linux-$_kernver"

prepare_isorec() {
	# https://wiki.postmarketos.org/wiki/Boot_process#isorec
	cp -v /usr/$(arch_to_hostspec $arch)/bin/busybox.static \
		"$builddir"/usr/
	cp -v "$srcdir"/init "$builddir"/usr/
	cp -v "$srcdir"/initramfs.list "$builddir"/usr/
}

prepare() {
	default_prepare
	prepare_isorec
	cp -v "$srcdir/$_config" .config
}

build() {
	unset LDFLAGS
	make ARCH="$_carch" CC="${CC:-gcc}" \
		KBUILD_BUILD_VERSION="$((pkgrel + 1 ))-postmarketOS"
}

package() {
	mkdir -p "$pkgdir"/boot
	make zinstall modules_install dtbs_install \
		ARCH="$_carch" \
		INSTALL_MOD_STRIP=1 \
		INSTALL_PATH="$pkgdir"/boot \
		INSTALL_MOD_PATH="$pkgdir" \
		INSTALL_DTBS_PATH="$pkgdir/usr/share/dtb"

	install -D "$builddir"/include/config/kernel.release \
		"$pkgdir/usr/share/kernel/$_flavor/kernel.release"
}

sha512sums="2b78607a2cafab6bd49c18cc4f79f26df3694f3984d6d24936dc722e72dca567f86eff3ae87f8560ed9bb363410a6ec7bccc33f3d1c9ab2bc3385be5f6a99da6  linux-5.11.8.tar.xz
384509349d552bbc1b42ce784080148f6b5e67060156df60c54aa767fbe7167ac030e0d8627281b0dc70ebc02f621edea2ebd2bd999b72f6b4bbd553ff2bacc1  config-postmarketos-exynos4.armv7
4bb111db4396a1981ad0883737c6594ca93701699846298b940d2ea202c666be0158a0f9ddc5b95b9147fa4cfe62639512ae78e9315f8d975b379ccc4e15da36  0001-ARM-decompressor-Flush-tlb-before-swiching-domain-0-.patch
ddee2a51b33b7bd0ba310085324e72342c992d6d047132f7204ff9695cef112d4809bbe734d382b720e8deb90b948b65709bbb407bd1a32876c524fe9f4bae67  0005-ARM-dts-exynos-Fix-charging-regulator-voltage-and-cu.patch
8eb1452fd2cc302df0ccc5bee33e99d12d6bb851fb48b281026f46b70008e3f515f3287f01684981ff81c7e2c422a9480a40e23f45823812f913cc866c319811  0006-ARM-dts-exynos-Add-top-off-charging-regulator-node-f.patch
dffe9934281e81b20e158ce9b80c6f1a91c5eaa06eb92ee655ee9430dba8aa066dd70a9fb163007f37556ea696ae0256a58f87408c8b7571d6d20f99c9492db2  0007-extcon-max8997-Add-CHGINS-and-CHGRM-interrupt-handli.patch
ea14ce70ac337b952b0246bb2765c7ca099808f4f46021b4f366e706265f9fab6a1e887e327d6b73d2b9963e4ae1f64a2877500cbc79a6214169486993f60069  0008-power-supply-max8997_charger-Set-CHARGER-current-lim.patch
b70e8afa3779d471835bbdda4c8ddcf882bce58752993314655596961210235558e87dce3d1f07810ab5bb3d0471e89933a8008d7b97d8ebd05a408ffc947054  0009-ARM-dts-exynos-Add-charger-supply-for-I9100.patch
11160ac085aa912439a18995e323dfc89dfc61d85f1221af8f0116eed0f5b1fd087fc2828174c8c60f937495c31d72240c2f3c4249c29ceda6acd92f08d235c4  0010-power-supply-max8997_charger-Switch-to-new-binding.patch
aaff0332b90e1f9f62de1128cace934717336e54ab09de46477369fa808302482d97334e43a85ee8597c1bcab64d3484750103559fea2ce8cd51776156bf7591  initramfs.list
09f1f214a24300696809727a7b04378887c06ca6f40803ca51a12bf2176a360b2eb8632139d6a0722094e05cb2038bdb04018a1e3d33fc2697674552ade03bee  init"
