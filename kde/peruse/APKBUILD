# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=peruse
pkgver=0_git20210402
pkgrel=0
_commit="bec24565782f34f3702e8f46493b7ab592c1ebef"
pkgdesc="A comic book viewer based on Frameworks 5, for use on multiple form factors"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://invent.kde.org/graphics/peruse"
license="LGPL-2.1-or-later AND LicenseRef-KDE-Accepted-LGPL"
depends="
	kirigami2
	okular-common-qml
	qt5-qtbase-sqlite
	qt5-qtimageformats
	qt5-qtquickcontrols
	"
makedepends="
	extra-cmake-modules
	kdeclarative-dev
	kfilemetadata-dev
	knewstuff-dev
	qt5-qtdeclarative-dev
	"
source="https://invent.kde.org/graphics/peruse/-/archive/$_commit/peruse-$_commit.tar.gz"
subpackages="$pkgname-creator"
options="!check" # No tests
builddir="$srcdir/$pkgname-$_commit"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

creator() {
	pkgdesc="Creation tool for comic books"

	amove usr/bin/perusecreator
	amove usr/share/metainfo/org.kde.perusecreator.appdata.xml
}

sha512sums="4df666377868d1120d0d660ea2585ab86b9d2a02476cbc15b7e27ee2ae2166f7172af39a2bf3649e6ac42d4903a92255bdcb989bf07a824dbc91f6bbb6fb2e8c  peruse-bec24565782f34f3702e8f46493b7ab592c1ebef.tar.gz"
